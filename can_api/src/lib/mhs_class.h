#ifndef __MHS_CLASS_H__
#define __MHS_CLASS_H__

#include "global.h"

#ifdef __cplusplus
  extern "C" {
#endif

#define OBJ_VALUE         1
#define OBJ_CAN           2
#define OBJ_CAN_FIFO      3
#define OBJ_CAN_RX_PUFFER 4
#define OBJ_CAN_TX_PUFFER 5

#ifdef __cplusplus
  }
#endif

#endif
