#ifndef __SER_IO_H__
#define __SER_IO_H__

#include "global.h"
#include "com_io.h"

#ifdef __cplusplus
  extern "C" {
#endif

extern const struct TComIoDriver SerDevice ATTRIBUTE_INTERNAL;

#ifdef __cplusplus
  }
#endif

#endif
