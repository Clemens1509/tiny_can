/******************/
/*  CMDLINE.H     */
/******************/

#ifndef _CMDLINE_H
#define _CMDLINE_H

void PrintHelp(void);
int ReadCommandLine(int argc, char **argv);

#endif


