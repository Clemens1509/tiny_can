#ifndef __STD_OP_H__
#define __STD_OP_H__

#include "hw_types.h"

void StdOpDlgExecute(struct TCanHw *hw, int pin_idx);

#endif
