#ifndef __MAIN_H__
#define __MAIN_H__

#include <gtk/gtk.h>
#include "can_types.h"
#include "gui.h"

#ifdef __cplusplus
  extern "C" {
#endif

void ExitApplikation(struct TMainWin *main_win);

#ifdef __cplusplus
  }
#endif

#endif

