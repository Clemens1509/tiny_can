#ifndef __CANDIAG_ICONS_H__
#define __CANDIAG_ICONS_H__

#include <gtk/gtk.h>

#ifdef __cplusplus
  extern "C" {
#endif

#define TINY_CAN_DIAG_ICONE       "tcan_diag.png"
#define TINY_CAN_M2_ICONE         "tcanm2.png"
#define TINY_CAN_M3_ICONE         "tcanm3.png"

#define OPEN_ICONE                "file_open.png"
#define WEB_ICONE                 "web.png"
#define TINY_CAN_M2_MANUAL_ICONE  "hardware_manuals.png"
#define TINY_CAN_M3_MANUAL_ICONE  "hardware_manuals.png"
#define SAMPLES_ICONE             "samples.png"

#ifdef __cplusplus
  }
#endif

#endif