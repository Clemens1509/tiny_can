#ifndef __HW_INFO_H__
#define __HW_INFO_H__

#ifdef __cplusplus
  extern "C" {
#endif

void ShowHardwareInfo(void);
void ShowInfoWin(char *info_string, const char *title, const char *win_title);

#ifdef __cplusplus
  }
#endif

#endif
