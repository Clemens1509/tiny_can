#ifndef __SETUP_H__
#define __SETUP_H__

#include "gui.h"

gint LoadConfigFile(struct TMainWin *main_win);
gint SaveConfigFile(struct TMainWin *main_win);

#endif
